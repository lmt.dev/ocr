﻿using System;

using System.Configuration;


using Lector_lib;

namespace LectorPatentes
{
    class Program
    {

        static void Main(string[] args)
        {
            Lector lector = new Lector();
            lector.Leer();
        }

        public event EventHandler<Lector.PatenteLeidaEventArgs> PatenteLeida;

        public void OnPatenteLeida(string patente)
        {
            EventHandler<Lector.PatenteLeidaEventArgs> handler = PatenteLeida;
            if (handler != null) handler(this, new Lector.PatenteLeidaEventArgs(patente));
        }
    }
}
