﻿using System;
using System.Threading;
using System.Drawing;
using openalprnet;
using OpenCvSharp;
using System.Configuration;

namespace Lector_lib
{
    public class Lector
    {
        public class PatenteLeidaEventArgs : EventArgs
        {
            public string Patente { get; set; }
            public Bitmap Foto { get; set; }
            public PatenteLeidaEventArgs(string patente)
            {
                Patente = patente;
            }
        }

        public void Leer()
        {
            var alpr = new AlprNet("us", "openalpr.conf", "runtime_data");
            if (!alpr.IsLoaded())
            {
                Console.WriteLine("OpenAlpr failed to load!");
                return;
            }

            int source = 0;
            //string source = "rtsp://192.168.100.61/user=admin&password=&channel=1&stream=0.sdp?real_stream";
            //string source = "rtsp://192.168.100.106:8080/h264_pcm.sdp";
            VideoCapture capture = new VideoCapture(source);
            using (Window window = new Window("Camera"))
            using (Mat image = new Mat()) // Frame image buffer
            {
                // When the movie playback reaches end, Mat.data becomes NULL.
                Boolean run = true;
                int indice = 1;
                int tomar_cada = 10;
                int tomadas = 0;
                int tomar = 3;
                while (run)
                {
                    capture.Read(image); // same as cvQueryFrame
                    if (image.Empty()) break;
                    window.ShowImage(image);
                    Cv2.WaitKey(30);

                    if (indice % tomar_cada == 0)
                    {
                        Bitmap bitmap_image = MatToBitmap(image);
                        //bitmap_image.Save(indice.ToString() + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg);
                        tomadas = tomadas + 1;
                        LeerPatente(alpr,bitmap_image);
                        //Thread thread = new Thread(() => LeerPatente(alpr, bitmap_image));
                        //thread.Start();
                    }
                    indice = indice + 1;
                }
            }
        }

        private void LeerPatente(AlprNet a, Bitmap img)
        {
            string resultado = "";
            var lectura = a.Recognize(img);
            int i = 0;
            foreach (var result in lectura.Plates)
            {
                //Console.WriteLine("Plate {0}: {1} result(s)", i++, result.TopNPlates.Count);
                //Console.WriteLine("  Processing Time: {0} msec(s)", result.ProcessingTimeMs);
                foreach (var plate in result.TopNPlates)
                {
                    //Console.WriteLine("DEBUG: "+plate.Characters);
                    if (plate.OverallConfidence > 85)
                    {
                        resultado = plate.Characters;

                        if (plate.Characters.Length == 6)
                        {
                            //@@@###
                            string patente_letras = resultado.Substring(0, 3);
                            string patente_numeros = resultado.Substring(3, 3);
                            patente_letras = NumeroALetra(patente_letras);
                            patente_numeros = LetraANumero(patente_numeros);
                            string patente = patente_letras + patente_numeros;                            
                            
                            if (validar_caracter(patente,"@@@###"))
                            {
                                Console.WriteLine(patente);
                               
                            }

                        }
                        else if (plate.Characters.Length == 7)
                        {
                            //@@###@@
                            string patente_letras_1 = resultado.Substring(0, 2);
                            string patente_numeros = resultado.Substring(2, 3);
                            string patente_letras_2 = resultado.Substring(5, 2);
                            string patente = patente_letras_1 + patente_numeros + patente_letras_2;
                            if (validar_caracter(patente, "@@###@@"))
                            {
                                Console.WriteLine(patente);
                                //disparar evento con patente e img(mapa de bits)
                                PatenteLeidaEventArgs lecturaPatente = new PatenteLeidaEventArgs(patente);
                                
                            }
                        }

                    }

                }
            }
        }

        private bool validar_caracter(string patente, string v)
        {
            Boolean result = true;
            int indice = 0;
            foreach(char caracter in patente)
            {


                if (v[indice] == (char)64)
                {
                    //Console.WriteLine("letra: "+ caracter);
                    result = result && Char.IsLetter(caracter);

                }
                else if(v[indice] == (char)35)
                {
                    //Console.WriteLine("numero: " + caracter);
                    result = result && Char.IsNumber(caracter);
                }
                indice = indice + 1;
            }

            
            return result;
        }

        private string LetraANumero(string s)
        {
            s.Replace("B", "8");
            s.Replace("O", "0");
            s.Replace("S", "5");
            s.Replace("Z", "2");
            s.Replace("A", "4");
            return s;
        }

        private string NumeroALetra(string s)
        {
            s.Replace("8", "B");
            s.Replace("0", "O");
            s.Replace("5", "S");
            s.Replace("2", "Z");
            s.Replace("4", "A");
            return s;
        }

        public static Bitmap MatToBitmap(Mat image)
        {
            return OpenCvSharp.Extensions.BitmapConverter.ToBitmap(image);
        } // end of MatToBitmap function
        
    }

}
